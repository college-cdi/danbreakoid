/**
 * Variables
 */

// Canvas variables
let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");

const gameState = {
    pause: "PAUSE",
    play: "PLAY",
    menu: "MENU",
    loading: "LOADING"
}

let menuReqAnimationId;


let reqAnimationId;
let changedAnimationReq = false;
let currentState = gameState.menu;

let assetsLoaded = 0;
let numbersOfAssetsToLoad = 4;
let paddle;
let ball;
let bricks;
let player;

init();

let img = new Image();
img.onload = function () {
    setTimeout(() => {
        anime ({
            targets: ['.first', '.last'],
            width: '0px',
            delay: function(el) {
                if (el.classList.contains('first')) {
                    return 0;
                } else {
                    return 1500;
                }
            },
            duration: 1500,
            loop: false,
            easing: 'easeInOutQuad'
        });
        currentState = gameState.menu;
        draw();
    }, 1000)
}
img.src = "../../assets/images/bg_niveau1.png";

const BACKGROUND_MENU_IMAGE = new Image();
BACKGROUND_MENU_IMAGE.onload = function () {
    currentState = gameState.play;
}
BACKGROUND_MENU_IMAGE.src = "../../assets/images/splashscreen_1100_720.png";


/**
 * event Listeners
 */

const keypresses = [];
const mouse = {x: 0, y: 0, isOnCanvas: false};

document.addEventListener("keydown", (event) => {
    if (event.code === 'Space') {
        keypresses.Space = true;
    } else {
        keypresses[event.key] = true;
    }
}, false);

document.addEventListener("keyup", (event) => {
    if (event.code === 'Space') {
        keypresses.Space = false;
    } else {
        keypresses[event.key] = false;
    }
    keypresses[event.key] = false;
}, false);

canvas.addEventListener("mousemove", (event) => {
    mouse.x = (event.pageX - canvas.offsetLeft) * canvas.width / 1200; // 1200px in css file.
    mouse.y = event.clientY;
    mouse.isOnCanvas = true;
})

canvas.addEventListener("mouseout", (event) => {
    mouse.isOnCanvas = false;
})

/**
 * Functions
 */

function draw() {
    chooseWhatToDraw();
    requestAnimationFrame(draw);
}

function chooseWhatToDraw() {
    switch(currentState) {
        case gameState.play:
            drawGame();
            return;
        case gameState.menu:
            drawMenu();
            return;
        case gameState.loading:
           setTimeout(() => {
            console.log("loading");
               // loading animation using anim.js
           }, 1000);
           currentState = gameState.menu;
           return;
        default:
    }
}

function drawGame() {
    console.log(currentState);
    ctx.drawImage(img, 0, 0, canvas.width, canvas.height)
    ball.draw(paddle);
    paddle.draw();
    bricks.draw();
    player.draw();
    ball.detectCollision(bricks, paddle, player);
}

function drawMenu() {
    console.log("Draw menu");
    ctx.drawImage(BACKGROUND_MENU_IMAGE, 0, 0, canvas.width, canvas.height);
    ctx.font = "16px Arial";
    ctx.fillStyle = "#0095DD";
    ctx.fillText("Press Enter to play", 90, 120);
    if (keypresses.Enter) {
        init();
        currentState = gameState.play;
    }
}

function loading() {
    console.log("loading");

    currentState = gameState.menu;
    // ctx.drawImage()
}

function init() {
    player = new Player();

    // Paddle variables
    paddle = new Paddle()
    
    // Ball variables
    ball = new Ball(paddle);
    
    // Brick variables
    // level 1
    let brickRowCount = 3;
    let brickColumnCount = 5;
    bricks =  new Bricks(brickRowCount, brickColumnCount);
    bricks.init();
    
}

function reset() {
     // Paddle variables
     paddle = new Paddle()
    
     // Ball variables
     ball = new Ball(paddle);
     
     // Brick variables
     // level 1
     let brickRowCount = 3;
     let brickColumnCount = 5;
     bricks =  new Bricks(brickRowCount, brickColumnCount);
     bricks.init();
}

/**
 * @TODO load images
 * @TODO Loading screen logic
 * @TODO menu
 * @TODO levels []
 */
