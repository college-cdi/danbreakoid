class Brick {
    width = 75;
    height = 10;
    padding = 2;
    offsetTop = 2;
    offsetLeft = 2;
    positionY;
    positionX;
    status = false;
    Id = 0;
    column;
    row;
    image;
    sprite;
    loaded = false;

    /**
     * @description Brick constructor.
     * @param {Integer} columnIndex Index of the column where the Brick will be positioned.
     * @param {Integer} rowIndex Index of the row where the Brick will be positioned.
     */
    constructor(columnIndex, rowIndex, brickColumnCount){
        this.Id = ++this.Id;
        this.width = (canvas.width/ brickColumnCount) - 2.5;
        this.positionX = (columnIndex*(this.width+this.padding))+this.offsetLeft;;
        this.positionY = (rowIndex*(this.height+this.padding))+this.offsetTop;
        this.column = columnIndex + 1;
        this.row = rowIndex + 1;
    }

    init(image) {
        this.image = image;

        const FRAMES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        this.sprite = {
            frames: FRAMES,
            width: this.image.width / FRAMES.length,
            height: this.image.height,
            currentFrame: Math.floor(Math.random() * FRAMES.length)
        }
    }

    /**
     * @description Collision Detection functionnality.
     * @param {Ball} ball Pointer to the Ball object.
     */
    detectCollision(ball, player) {
        if ( !this.status && ball.positionX > this.positionX && ball.positionX < this.positionX + this.width && ball.positionY > this.positionY  && ball.positionY < this.positionY + this.height) {
            this.status = true;
            ball.dy = -ball.dy;

            /**
             * @todo animate Score.
             */ 
            player.score += 100;
        }
        return;
    }

    /**
     * @description Draws a single brick.
    */
    draw() {
        if (!this.status) {
            ctx.drawImage(this.image, this.sprite.width * this.sprite.currentFrame , 0, this.sprite.width, this.sprite.height, this.positionX, this.positionY, this.width, this.height)
        }
    }

}