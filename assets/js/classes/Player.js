
class Player {
    score;
    hp;
    image;
    currentFrame;
    sprite;
    width = 100;
    height = 30;

    constructor() {
        this.score = 0;
        this.hp = 3;
        this.image = new Image();
        this.image.src = "../../../assets/images/sprite_barre_de_vies.png";

        const FRAMES = [1, 2, 3, 4]
        this.sprite = {
            frames: FRAMES,
            width: this.image.width / 4,
            height: this.image.height,
        }
        console.log(this.sprite);
        console.log(this.image.width);
    }

    setScore(score) {
        this.score = score;
    }

    setHp(hp) {
        this.hp = hp;
    }

    drawScore() {
        ctx.font = "16px Arial";
        ctx.fillStyle = "#0095DD";
        ctx.fillText("Score: " + this.score, 5, 20);
    }   

    loseLive(ball, paddle) {
        this.hp -= 1;
        if (this.hp <= 0) {
            console.log("Game over!");
            currentState = gameState.menu;
        }
        console.log(this.hp);
        return true;
    }

    drawhp() {
        let currentFrame = 3 - this.hp
        ctx.drawImage(this.image, this.sprite.width * currentFrame, 0, this.sprite.width, this.sprite.height, canvas.width -  (this.sprite.width / 5 + 5), 0, this.sprite.width / 5, this.sprite.height / 5)
    }

    draw() {
        this.drawhp();
        this.drawScore();
    }
}