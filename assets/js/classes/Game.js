class Game {
    ball;
    paddle;
    bricks;
    player;
    reqAnimationId;
    levels = [1, 2, 3];
    state = {
        Menu : "MENU",
        Play : "PLAY",
        Pause : "PAUSE",
        loading: "LOADING"
    }
    
    constructor() {
        // Paddle variables
        this.paddle = new Paddle()
        
        // Ball variables
        this.ball = new Ball(this.paddle);
        
        // Brick variables
        // level 1
        let brickRowCount = 3;
        let brickColumnCount = 5;
        this.bricks =  new Bricks(brickRowCount, brickColumnCount);
        this.bricks.init();
        
        this.player = new Player();
    }

    load() {}

    init() {
        this.state = this.state.Play;
    }

    drawMenu() {}

    drawGame() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        this.ball.draw(this.paddle);
        this.paddle.draw();
        this.bricks.draw();
        this.player.drawScore();
        this.ball.detectCollision(this.bricks, this.paddle, this.player);
        requestAnimationFrame(this.drawGame);
    }

    play() {}

    pause() {}

    exitToMainMenu() {}

    draw() {
        console.log(this.state);
        switch(this.state) {
            case(this.state.Menu):
            this.showMenu();
                return;
            case("Play"):
            console.log("test");
                this.drawGame();
                this.reqAnimationId = requestAnimationFrame(this.drawGame);
                // return;
            case(this.state.Pause):
                return;
            default:
                return;
        }
        // case 'levels' draw level();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }

}