class Ball {
    positionX;
    positionY;
    speed = 2;
    dx = this.speed;
    dy = -this.speed;
    radius = 10;
    HasBeenInitiated = false;
    image

    /**
     * @description Ball constructor.
     * @param {Paddle} paddle Pointer to the object.
     */
    constructor(paddle) {
        this.positionX = (paddle.positionX - this.radius) + paddle.width/2;
        this.positionY = paddle.positionY - this.radius;
        this.image = new Image()
        this.image.onload = function () {

        }
        this.image.src = "../../../assets/images/ball2.png";
    }

    /**
     * @description Detect if the ball hits something.
     * @param {Bricks} bricks pointer to the Bricks object.
     * @param {Paddle} paddle pointer to the Paddle object.
     * @param {Player} player pointer to the Player object.
     */
    detectCollision(bricks, paddle, player) {
        bricks.detectCollision(this, player);
        let hasHitTopOfCanvas =  (this.positionY + this.dy < this.radius);
        let hasHitPaddle = (this.positionY + this.dy > paddle.positionY && this.positionY + this.dy < paddle.positionY + paddle.height && this.positionX > paddle.positionX && this.positionX < paddle.positionX + paddle.width)
        
        if ( hasHitTopOfCanvas  || hasHitPaddle ) {
            this.dy = -this.dy;
        } else if ( this.positionY + this.dy > canvas.height + this.radius ) {
            player.loseLive(this);
            this.reset(paddle);
            // document.location.reload();
        }
        if ( this.positionX + this.dx < this.radius || this.positionX + this.dx > canvas.width - this.radius) {
            this.dx = -this.dx;
        }
    }

    reset(paddle) {
        this.dy = -this.speed;
        this.positionX = (paddle.positionX - this.radius) + paddle.width/2;
        this.positionY = paddle.positionY - this.radius;
        this.HasBeenInitiated = false;
    }

    /**
     * @description Moves the ball.
     */
    move(paddle) {
        if (currentState == gameState.pause) return;
        if (this.HasBeenInitiated) {
            this.positionX += this.dx;
            this.positionY += this.dy;
        } else {
            this.positionX = paddle.positionX + paddle.width/2;
        }

        if (keypresses.Space) {
            this.HasBeenInitiated = true;
        }
    }

    /**
     * @description Draws the ball on the canvas.
     */
    draw(paddle) {
    ctx.drawImage(this.image, this.positionX, this.positionY, this.radius, this.radius);
    this.move(paddle);
    }
}
