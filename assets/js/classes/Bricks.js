class Bricks {
    items = [];
    rowCount = 3;
    columnCount = 5;
    loaded = false;
    maxScore;
    
    /**
     * @description Brick List constructor
     * @param {Integer} rowCount number of rows
     * @param {Integer} columnCount number of columns
     */
    constructor(rowCount, columnCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;

        for (var c = 0; c < this.columnCount; c++) {
            for (var r = 0; r < this.rowCount; r++) {
                let brick = new Brick(c, r, columnCount);
                this.items.push(brick)
                }
            }
        this.maxScore = player.score + this.items.length * 100;
    }

    init() {
        this.image = new Image();
        this.image.onload = function () {
            this.loaded = true;
        }
        this.image
        this.image.src = "../../../assets/images/sprite_briques.png";
        this.items.forEach((item) => {
            item.init(this.image);
        })
    }

    initBricks() {
        
    }
    /**
     * @description Detect if one brick within the list is hit by the ball.
     * @param {Ball} ball Pointer to the ball object.
     */
    detectCollision(ball, player) {
        this.items.forEach(brick => {
            brick.detectCollision(ball, player);
            if (player.score >= this.maxScore) {
                console.log("WIN");
                this.maxScore += this.items.length * 100;
                reset();
            }
        })
    }

    /**
     * @description Draws all bricks from its list.
    */
    draw() {
        this.items.forEach(brick => {
            brick.draw();
        })
    }
    
}