class Paddle {
    positionX;
    positionY;
    height = 10;
    width = 75;
    padding;
    image;
    /**
     * @description Paddle constructor.
     */
    constructor() {
        this.positionX = (canvas.width - this.width)/2;
        this.positionY = canvas.height - this.height*2;
        this.image = new Image()
        this.image.onload = function () {
        }
        this.image.src = "../../../assets/images/paddle.png";
    }

    /**
     * @description moves the paddle on the canvas.
     */
    move() {            
        if (currentState === gameState.pause) return;
        if (this.positionX > 0 && (keypresses.a || keypresses.ArrowLeft | (mouse.isOnCanvas && mouse.x < this.positionX + this.width / 2 - 7)) ) {
            this.positionX -= 4;
        } else if (this.positionX < canvas.width - this.width && (keypresses.d || keypresses.ArrowRight | (mouse.isOnCanvas && mouse.x > (this.positionX + this.width / 2 ) )) ) {
            this.positionX += 4;
        }
    }

    /**
     * @description Draws the paddle.
     */
    draw() {
    ctx.drawImage(this.image, this.positionX, this.positionY, this.width, this.height);
    this.move();
    }
}